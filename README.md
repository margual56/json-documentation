# JSON to HTML for documentation
This project aims to tranform a JSON into a standalone website with extra fields to be able to document the JSON, which otherwise is not possible.

## Note:
This is not meant for data JSONs, it's for `key: value` JSONs **only**, which means that, for example, `"dataset": [{ ... }, { ... }]` will break the look and functionality.
