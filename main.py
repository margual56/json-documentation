import json
import numbers
import sys
from os import path
from typing import TextIO


def write(d: dict[any, any], level: int, file: TextIO):
    for key, value in d.items():
        if type(value) == dict:
            default_value = "(See schema &#128071;)"
        elif type(value) == str:
            default_value = f"'{value}'"
        else:
            default_value = str(value)

        file.write(f"""
    <details>
        <summary><h{level} class="key">{key}</h{level}></summary>
        <ul>
            <li>
                <b>Description:</b>
                <p class="description"></p>
             </li>
            <li><b>Type: </b> {type(value).__name__}</li>
            <li><b>Default value:</b> {default_value}</li>
            """)

        if type(value) == dict:
            file.write(f"<li><b>Schema:</b><br/>")
            write(value, level+1, file)
            file.write("</li>")
        elif isinstance(value, numbers.Number):
            file.write(f"<li><b>Min:</b> </li>\n")
            file.write(f"<li><b>Max:</b> </li>")

        file.write(f"""
        </ul>
    </details>
""")


def main(file_in: str, file_out: str):
    with open(file_in, 'r') as i, open(path.join("html", file_out), 'w') as out:
        out.write(f"""
<html>
    <head>
      <link rel="stylesheet" href="style.css">
    </head>
    <body>
""")
        j = json.load(i)

        write(j, 1, out)

        out.write(f"""
    </body>
</html>""")


if __name__ == '__main__':
    if len(sys.argv) < 2:
        sys.stderr.write(f"Incorrect number of arguments!\n")
        sys.stderr.write(f"\tUsage: {sys.argv[0]} <config.json> [output.html]")
        sys.exit(1)

    if len(sys.argv) < 3:
        main(sys.argv[1], "index.html")
    else:
        main(sys.argv[1], sys.argv[2])
